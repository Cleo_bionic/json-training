<?php
error_reporting(0);
  /*
  * Contains sample students data
  */

  $student_data = array(
    "10/12345" => array("fullname" => "Abubakar Ango", "class"=>"HND 2", "state" => "Kwara", "specialization" => "DevOps"),
    "11/12345" => array("fullname" => "Nokot Boryo", "class"=>"500 Level", "state" => "Gombe", "specialization" => "Frontend"),
    "12/12345" => array("fullname" => "Emmanuel Dauda", "class"=>"600 Level", "state" => "Bauchi", "specialization" => "Mobile"),
    "13/12345" => array("fullname" => "Auwal", "class"=>"300 Level", "state" => "Kaduna", "specialization" => "Sys Admin")
  );

  if(!isset($_GET['regnum'])){
    echo "Error!";
  }else{

      echo json_encode($student_data[$_GET['regnum']]);

}

?>
